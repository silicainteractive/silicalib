﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SilicaLib.Utils {
    public class SilU {
        static Keys? justPressed;
        static KeyboardState state;

        public static void Update() {
            state = Keyboard.GetState();

        }
        public static bool IsKeyDown(Keys key) {
            if (state.IsKeyDown(key))
                return true;
            return false;
        }
        public static bool IsKeyUp(Keys key) {
            if (state.IsKeyUp(key))
                return true;
            return false;
        }
        public static bool KeyJustPressed(Keys key) {
            if ((key != justPressed) && state.IsKeyDown(key)) {
                justPressed = key;
                return true;
            }
            if ((key == justPressed) && state.IsKeyUp(key)) {
                justPressed = null;
            }
            return false;
        }
    }
}
