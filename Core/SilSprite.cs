using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace SilicaLib.Core {
    public class Animation {
        public readonly float fps;
        public readonly int[] frames;
        public Animation(int[] frames, float fps) {
            this.frames = frames;
            this.fps = fps;
        }
    }
    public class SilSprite : DrawableGameComponent {
        protected Texture2D image;
        protected Vector2 position;
        protected Rectangle drawRect;
        protected Vector2 size;
        protected Vector2 frameCount;
        protected int currentFrameX;
        protected int currentFrameY;
        
        protected Dictionary<string, Animation> animations;

        private string imageString;
        private int animIndex;
        private float elapsedTime;
        private Animation currentAnimation = null;

        protected float Width {
            get { return size.X; }
        }
        protected float Height {
            get { return size.Y; }
        }


        public SilSprite(Vector2 position, string assetName)
            : base(SilGame.INSTANCE) {
            this.position = position;
            this.imageString = assetName;
            animations = new Dictionary<string, Animation>();
            currentFrameX = currentFrameY = 0;
            frameCount = Vector2.Zero;
        }
        public SilSprite(Vector2 position)
            : this(position, "") {
        }

        /// <summary>
        /// Use this method for sprite sheets. For simple graphics use the constructor
        /// </summary>
        /// <param name="assetName"></param>
        /// <param name="size">The size of a frame</param>
        /// <returns>The Texture2D returned by the Content.Load() </returns>
        public Texture2D LoadGraphic(string assetName, Vector2 size) {
            image = (Texture2D)Game.Content.Load<Texture2D>(assetName);
            imageString = assetName;
            this.size = size;
            drawRect = new Rectangle((int)(currentFrameX * size.X), (int)(currentFrameX * size.Y), (int)size.X, (int)size.Y);
            frameCount = new Vector2(image.Bounds.Width / size.X, image.Bounds.Height / size.Y);
            return image;
        }
        public void AddAnimation(string key, int[] frames, int fps) {
            animations.Add(key, new Animation(frames,1.0f/(float)fps));
        }

        public void Play(string key) {
            currentAnimation = animations[key];
        }

        public override void Initialize() {
            base.Initialize();

            animIndex = 0;
        }
        protected override void LoadContent() {
            base.LoadContent();

            if (imageString != "")
                image = (Texture2D)Game.Content.Load<Texture2D>(imageString);
        }
        protected override void UnloadContent() {
            base.UnloadContent();
        }
        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
            elapsedTime += (float) gameTime.ElapsedGameTime.TotalSeconds;
            drawRect.X = (int)(currentFrameX * size.X);
            drawRect.Y = (int)(currentFrameY * size.Y);
            if (currentAnimation != null) {
                if (elapsedTime >= currentAnimation.fps) {
                    currentFrameX = currentAnimation.frames[animIndex++];
                    if (animIndex == currentAnimation.frames.Length)
                        animIndex = 0;
                    elapsedTime = 0;
                }
            }
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch sb) {
            base.Draw(gameTime);
            if (image != null) {
                sb.Draw(image, position, drawRect, Color.White);
            }
        }
    }
}
