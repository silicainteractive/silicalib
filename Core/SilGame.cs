using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using SilicaLib.Utils;
namespace SilicaLib.Core {
    public class SilGame : Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SilState _currentState;
        internal static SilGame INSTANCE;

        public SilGame(Type initialState) {
            if (initialState.BaseType != typeof(SilState)) {
                throw new TypeLoadException("Parameter must be of type SilState");
            }
            
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            INSTANCE = this;
            _currentState = (SilState) Activator.CreateInstance(initialState);
        }
        
        protected override void Initialize() {
            _currentState.Create();
            base.Initialize();
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent() {

        }

        public static void SetState(SilState newState) {
            SilState _previousState = INSTANCE._currentState;
            INSTANCE._currentState = newState;
            INSTANCE._currentState.OnStateChanged(_previousState);
            INSTANCE._currentState.Create();
        }

        protected override void Update(GameTime gameTime) {
            _currentState.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            _currentState.Draw(gameTime);
            base.Draw(gameTime);
        }
        public static void ExitGame() {
            SilGame.INSTANCE.Exit();
        }
        public static GraphicsDevice GetGraphics() {
            return INSTANCE.GraphicsDevice;
        }
    }
}
