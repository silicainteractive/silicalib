﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SilicaLib.Utils;

namespace SilicaLib.Core {
    public class SilState: GameComponent{
        private SilState _lastState;
        protected SpriteBatch spriteBatch;
        protected List<SilSprite> children;
        public SilState(): base(SilGame.INSTANCE) {
            Enabled = false;
            children = new List<SilSprite>();
        }
        
        public virtual void Create() {
            spriteBatch = new SpriteBatch(SilGame.GetGraphics());
        }
        public void AddChild(SilSprite child){
            children.Add(child);
        }
        public void OnStateChanged(SilState s) {
            _lastState = s;
        }
        public override void Update(GameTime gameTime) {
            SilU.Update();
            if (SilU.KeyJustPressed(Keys.Escape)) {
                SilGame.ExitGame();
            }
            foreach (SilSprite sprite in children) {
                sprite.Update(gameTime);
            }
        }
        public virtual void Draw(GameTime gameTime) {
           // throw new NotImplementedException("SilState.Draw() cannot be called, do you call a base.Draw() somewhere?");
            spriteBatch.Begin();
            foreach (SilSprite sprite in children) {
                sprite.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();
        }
    }
}
